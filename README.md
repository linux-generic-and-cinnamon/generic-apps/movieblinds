# Movieblinds

Basically this project creates by default a blank window in full-screen.		
It can be switched to small resizable window through the Preferences dialog.		

Window can have one of three colors: black, grey, or white.		
Additionally there is a fourth user-customizable color.

Intended usage is for having a distraction-free environment while watching		
movies in windowed mode. It can also serve as background for screenshots.		

Please note the window is **NOT** always-on-top so any system notifications		
or other popups will still appear on screen.		

Also the panel(s) will remain visibile just in case the window cannot be dismissed.		
This may be made optional in a future version.

Showing/hiding the window can be done either through left-click or scroll over the tray icon.		
Depending on the chosen indicator system either one, the other or both methods may be available.		
Window can also be disabled by scrolling mouse wheel once over its surface.		
Changing window color can be done either through middle-click on icon (where available)		
or using the context menu. Function is also available on the window itself.

The project sets **Gtk.StatusIcon** as default, as it offers the most functionality.		
Other indicator can be enabled by selecting from the list in the **Preferences** dialog.		
There are four available tray indicator systems:

-  Gtk.StatusIcon
-  XApp.Indicator
-  AppIndicator3.Indicator
-  AyatanaAppIndicator3.Indicator

Please note each of the indicators has its own range of available functions;		
one may miss scrolling, other may miss different left/right-click functions,		
other may miss middle-click and so on. So far **Gtk.StatusIcon** is the only one		
providing a full range of functions. And, as expected, it has been deprecated		
because the so-called developers of the new wave can't deal with software complexity.

Main script is installed by default in **/usr/lib/movieblinds**.		
Its launcher is in **/usr/bin**.

Testing was so far performed solely on a Linux Mint 19.2 Cinnamon system.

That's all. Enjoy!

**© Drugwash, 2024.02.23**